function h = ROIfromClick(x,y,imMat, ifig ) %, dfig)

r = 20;
deadzoneOffset = 4;
penumbraOffset = 4;

xxx = testimprofile( imMat, x, y, r );

figure(ifig)

factor = sqrt(0.5); % conversion from hypoteneuse to catheti
offset =[0,-1; factor,-factor; 1,0; factor,factor; 0,1; -factor,factor; -1, 0; -factor,-factor];
msize = 1; 
lsize = 1;
h.ROI = xxx;
h.handles.ROI = drawpolygon(gca,'Position', xxx, 'FaceAlpha', 0, 'LineWidth', lsize, 'MarkerSize', msize );

deadzone = xxx + deadzoneOffset * offset;
h.deadzone = deadzone;
h.handles.deadzone = drawpolygon(gca,'Position', deadzone, 'LineWidth', lsize, 'MarkerSize', msize, 'Color', [1,1,1], FaceAlpha=0 );

penumbra = deadzone + penumbraOffset * offset;
h.penumbra = penumbra;
h.handles.penumbra = drawpolygon(gca,'Position', penumbra, 'LineWidth', lsize, 'MarkerSize', msize,  'Color', [1,0,1], FaceAlpha=0 );

end