
function [handles,stats] = click2ROI( im, stack )
% im is an image you want to use for picking ROIs

tic
%df = figure; % target for data plot
imgf = figure; % ( 'ButtonDownFcn', @figCallBackFcn );
imgo = imshow( im );
clim( 'auto' );
axis image
axis on
hold on
n=10;
clear handles
for i=1:n
    [x,y]=ginput(1);
    if size([x,y])>0
        handles(i) = ROIfromClick(y,x, im, imgf );
        stats(i) = roi2stats3D( stack, handles(i) );
    else
        break
    end
end

toc
end
