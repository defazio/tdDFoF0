%compare to max projections
function downsizeStack = comp2max( Stack1, Stack2 )
tic
pmRaw = projectMax( Stack1 );
pmRaw = histeq(pmRaw);

pmds = projectMax( Stack2 );
pmds = histeq(pmds);
figure
montage({pmRaw pmds});
toc
end