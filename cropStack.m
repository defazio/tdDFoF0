% crop in x-y plane: get crop from max/sd projection, crop stack
function cstack = cropStack( stack )
maxpro = projectMax( stack );
maxpro = histeq( maxpro );
[ cmp, J ] = imcrop( maxpro ); % J [ xstart, ystart, width, height ]
Jround = round(J);
height = Jround(4); % size( series1{1,1}, 1 );
width = Jround(3); % size( series1{1,1}, 2 );
rs = Jround(1); % row start and end
re = rs + width-1;
cs = Jround(2); % column start and end
ce = cs + height-1;
frames = size( stack, 3 );
cstack = zeros(height, width, frames, 'uint16');
tic
%for p = 1 : frames
     cstack(:,:,:) = stack( cs:ce, rs:re, : ); 
%end
toc
CropMaxProj = max( cstack, [], 3 );
%CropMaxProj2 = histeq( CropMaxProj );
montage( { cmp, CropMaxProj } );
clim('auto');
end