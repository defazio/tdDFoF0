function export2igor( statsStruct,fname )

ncells = size( statsStruct, 2 );
npoints = size( statsStruct(1).dFoF0.ROI, 1);

pen = zeros( npoints, ncells );
dz = zeros( npoints, ncells );
roi = zeros( npoints, ncells );
pensub = zeros( npoints, ncells );
roidf = zeros( npoints, ncells );
pendf = zeros( npoints, ncells );
pensubdf = zeros( npoints, ncells );
for i=1:ncells
    roi( :, i ) = statsStruct(i).ROI.meanZ(:);
    dz( :, i ) = statsStruct(i).deadzone.meanZ(:);
    pen( :, i ) = statsStruct(i).penumbra.meanZ(:);
    pensub( :, i ) = roi(:,i)-pen(:,i);

    roidf( :, i ) = statsStruct(i).dFoF0.ROI(:);
    pensubdf( :, i ) = statsStruct(i).dFoF0.pensub(:);
    %pendf( :, i ) = statsStruct(i).dFoF0.pen(:);


end
save(fname, 'roi', 'dz', 'pen');
end