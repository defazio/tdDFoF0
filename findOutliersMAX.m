function [out, Amax] = findOutliersMAX( A, factor )
% factor = 5 seems to work at first
tic
    Amax = projectMax(A);
    %sx = size( Amax, 1 );
    %sy = size( Amax, 2 );
    %sz = size( A, 3 );
%    out = zeros( sx, sy, 'logical' );
    maxmean = mean( Amax, 'all' );
    maxstd = std( double(Amax),0,'all');
%    out = isoutlier( double(A), 'percentile', [0 2] ); %, 'linear' );
    out = Amax > (maxmean + factor * maxstd);
%    out = isoutlier( double(Amax), 'movmedian', factor );
    imshowpair( Amax, out );
    clim('auto');
    disp(append('find outliers...', num2str(toc)));
end
