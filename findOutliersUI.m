
function MaxProj = findOutliersUI( stack )
% max projection, sd projection?
tic
MaxProj = max( stack, [], 3 );
%MaxProj2 = histeq( MaxProj );
%df = figure; % target for data plot
imgf = figure;
mp = imshow( MaxProj );
clim( 'auto' );
dcm = datacursormode; % use cursor to id pixel for plot
dcm.Enable = 'on';
dcm.Updatefcn = {@outlierZero MaxProj stack imgf };
toc
end

%MeanProj = mean( stack1, 3);
%StdProj = std( single(stack1), 0, 3);

% StdProj = std( stack1, 0, 3 ); % this is not how std works!
% StdProj2 = histeq( StdProj );
% figure
% imshowpair( MaxProj2, StdProj2 );