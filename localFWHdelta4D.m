function eightPairs = localFWHdelta4D( immat, xx, yy, win )
eightPairs = zeros(8,2);
% return 8 pairs of coordinates for freehand ROI
% 1,3,5,7 are orthogonal with x-y axes of image
% 2,4,6,8 are rotated 45 degrees
% each pair is based on FWHdelta, full width at half delta
% where delta = max - min of image subregion 
% defined by win and location of click

% img is a 2d image array; note smoothing in preprocessing
% xx, yy is the location of a click for an ROI
% win is the 0.5 search width for local max and min

% from imageanalyst
% https://www.mathworks.com/matlabcentral/answers/310113-how-to-find-out-full-width-at-half-maximum-from-this-graph
% Find the half max value.
% y direction
%data = zeros(1, win);

% NEEDS ERROR CHECKING NEAR EDGES of image!!!
% get size of image
% check that xx and yy +/- win don't cross the edge

% along the y - axis
data = immat( xx, yy-win:yy+win );
halfMax = ( min( data, [], 2 ) + max( data, [], 2 )) / 2;
% Find where the data first drops below half the max.
index1 = find( data >= halfMax, 1, 'first' );
% Find where the data last rises above half the max.
index2 = find( data >= halfMax, 1, 'last' );

eightPairs(1,2) = index1;
eightPairs(5,2) = index2;
eightPairs(1,1) = xx;
eightPairs(5,1) = xx;

% x direction
%data = zeros( win, 1 );
data = immat( xx-win:xx+win, yy );
halfMax = ( min( data, [], 1 ) + max( data, [], 1 )) / 2;
% Find where the data first drops below half the max.
index1 = find( data >= halfMax, 1, 'first' );
% Find where the data last rises above half the max.
index2 = find( data >= halfMax, 1, 'last' );

eightPairs(3,1) = index1;
eightPairs(7,1) = index2;
eightPairs(3,2) = yy;
eightPairs(7,2) = yy;

% repeat with the image rotated 45 degrees
% along the y - axis

immatRot = immat;

data = immatRot( xx, yy-win:yy+win );
halfMax = ( min( data, [], 2 ) + max( data, [], 2 )) / 2;
% Find where the data first drops below half the max.
index1 = find( data >= halfMax, 1, 'first' );
% Find where the data last rises above half the max.
index2 = find( data >= halfMax, 1, 'last' );

eightPairs(2,2) = index1;
eightPairs(6,2) = index2;
eightPairs(2,1) = xx;
eightPairs(6,1) = xx;

% x direction
data = immatRot( xx-win:xx+win, yy );
halfMax = ( min( data, [], 1 ) + max( data, [], 1 )) / 2;
% Find where the data first drops below half the max.
index1 = find( data >= halfMax, 1, 'first' );
% Find where the data last rises above half the max.
index2 = find( data >= halfMax, 1, 'last' );

eightPairs(4,1) = index1;
eightPairs(8,1) = index2;
eightPairs(4,2) = yy;
eightPairs(8,2) = yy;
%eightPairs = eightPairs(1,1);
%disp("leaving localfwhm")
end