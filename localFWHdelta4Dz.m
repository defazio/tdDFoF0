function eightPairs = localFWHdelta4Dz( immat, imRot, xx, yy, win )
eightPairs = zeros(4,2);
% return 8 pairs of coordinates for freehand ROI
% 1,3,5,7 are orthogonal with x-y axes of image
% 2,4,6,8 are rotated 45 degrees
% each pair is based on FWHdelta, full width at half delta
% where delta = max - min of image subregion 
% defined by win and location of click

% img is a 2d image array; note smoothing in preprocessing
% xx, yy is the location of a click for an ROI
% win is the 0.5 search width for local max and min

% from imageanalyst
% https://www.mathworks.com/matlabcentral/answers/310113-how-to-find-out-full-width-at-half-maximum-from-this-graph
% Find the half max value.
% y direction
%data = zeros(1, win);

% NEEDS ERROR CHECKING NEAR EDGES of image!!!
% get size of image
% check that xx and yy +/- win don't cross the edge
% range idea from : https://www.mathworks.com/matlabcentral/answers/62208-how-do-i-find-a-min-max-value-within-a-defined-range-for-each-column-of-a-matrix#answer_344118

% along the y - axis
range = yy-win:yy+win;
data = immat( xx, range );
halfMax = ( min( data, [], 2 ) + max( data, [], 2 )) / 2;
% Find where the data first drops below half the max.
index1 = find( data >= halfMax, 1, 'first' );
% Find where the data last rises above half the max.
index2 = find( data >= halfMax, 1, 'last' );

eightPairs(1,1) = range(index1);
eightPairs(5,1) = range(index2);
eightPairs(1,2) = xx;
eightPairs(5,2) = xx;

% x direction
%data = zeros( win, 1 );
range = xx-win:xx+win;
data = immat( range, yy );
halfMax = ( min( data, [], 1 ) + max( data, [], 1 )) / 2;
% Find where the data first drops below half the max.
index1 = find( data >= halfMax, 1, 'first' );
% Find where the data last rises above half the max.
index2 = find( data >= halfMax, 1, 'last' );

eightPairs(3,2) = range(index1);
eightPairs(7,2) = range(index2);
eightPairs(3,1) = yy;
eightPairs(7,1) = yy;

% now do the rotated image
% along the y - axis
range = yy-win:yy+win;
data = imRot( xx, range );
halfMax = ( min( data, [], 2 ) + max( data, [], 2 )) / 2;
% Find where the data first drops below half the max.
index1 = find( data >= halfMax, 1, 'first' );
% Find where the data last rises above half the max.
index2 = find( data >= halfMax, 1, 'last' );

eightPairs(2,1) = range(index1);
eightPairs(6,1) = range(index2);
eightPairs(2,2) = xx;
eightPairs(6,2) = xx;

% x direction
%data = zeros( win, 1 );
range = xx-win:xx+win;
data = imRot( range, yy );
halfMax = ( min( data, [], 1 ) + max( data, [], 1 )) / 2;
% Find where the data first drops below half the max.
index1 = find( data >= halfMax, 1, 'first' );
% Find where the data last rises above half the max.
index2 = find( data >= halfMax, 1, 'last' );

eightPairs(4,2) = range(index1);
eightPairs(8,2) = range(index2);
eightPairs(4,1) = yy;
eightPairs(8,1) = yy;
end