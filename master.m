% master
% open dataset
% run openETS, extracts image stack into memory
disp("reading ETS and VSI...")
[ rstack, name ] = openETSui(); % puts image stack into 'stack'
disp("ETS read into memory...")
%% cuts off first 10s, 100 frames with fast transient
disp("trimming started...")
stack = trimArray(rstack,3,1,100);
disp("trimming completed...")
%% clean stack
disp('cleaning stack')
[ outliers, projMax ] = findOutliersMAX( stack, 4 ); % 4.5 x std threshold
stack = replaceOutliers( stack, outliers );
disp("cleaning completed")
%% gaussian
disp("spatGauss")
stack = spatGauss( stack, 0.5, 5 );
disp("spatGauss completed")
%% run tdResize, bin stack, 4x4 again seems fine!
disp('resize, 4x4 binning')
bin = 2;
stack = tdResize( stack, bin );
disp("spatial resize completed")
%% run cropstack - project max, cut as small as possible
disp('cropstack')
stack = cropStack( stack ); % user must right click and crop
disp("crop completed")
%% skip resize in time
disp('downsampling in time')
%zst = cst4x4; % 
stack = resizeZ( stack, 2 ); % downsample by 10 frames
disp("downsampling completed")
%% full frame dF/F0
%% bar F of T
%t1 = 10; % frames to smooth
%bFofT = BarFofT( zst, t1 );
%% min Bar F of T
%t2 = 500; % frames to seek backwards minimum
%minBF = minBarF( bFofT, t2 );
%% dF/F0
disp('dFoF0')
t1=50; % 10s?, frames, 0.1 sec / frame right now!
t2=inf; % frames, should be 500s but MinBarF is really slow! 
t4=200; % frames to define a prominent event for slope fill
[dFoF0,minBF] = deltaFoverFzero( stack, t1, t2, t4 ); % includes t1 and t2 steps above!
disp("dFoF0 completed")
% store the analyzed data!
%createH5( dFoF0, name, '/1' );
%% smooth stack
sDFoF0 = SGtStack( dFoF0, 3, 9 );
%% MOVIE TIME!
disp('make a movie')
movieTime( sDFoF0 );
%%
mj = projectMaxAnalysis(sDFoF0,minBF,stack);
%%
[ meanDFoF0, maxDFoF0, stdDFoF0 ] = projMeanMaxSTD(sDFoF0);
[ meanStack, maxStack, stdStack ] = projMeanMaxSTD(stack);
%%
save matlab.mat -v7.3