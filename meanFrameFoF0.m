% calculate mean frame F0 dFoF0
function FoMeanF = meanFrameFoF0(stack)
% assumes stack is height x width x frames
tic
%height = size( stack, 1);
%width = size( stack, 2);
frames = size( stack, 3);
FoMeanF = double(stack);
for f = 1 : frames
    frame = double(stack(:,:,f));
    meanf = mean( frame, 'all' );
    FoMeanF(:,:,f) = frame / meanf;
end
disp(append('dF over mean F ...',num2str(toc)));
end