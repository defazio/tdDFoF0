% min bar F
function minBF = minBarF( barFofT, t2, t4 )
% for each time point from the end, 
% look backwards in time over t2 to find the minimum barFofT
% t2 = 500;
height = size( barFofT, 1);
width = size( barFofT, 2);
frames = size(barFofT, 3);
minBF = zeros( height, width, frames);
%linex = linspace( 0, frames, frames );
tic
%for h = 1 : 1 : height
%    for w = 1 : 1 : width 
% linear indexing
pixels = height * width;
sz = [ height width ];
parstor = zeros(pixels,frames);
for p=1:pixels
    [h,w]=ind2sub(sz,p);
    parstor(p,:)=barFofT(h,w,:);
end
%f=figure;
parfor p = 1 : pixels
        %[h, w] = ind2sub(sz,p);            
        PofT = parstor(p,:); %barFofT( h, w, : );
        PofT = squeeze(PofT); % = reshape( PofT, frames);
        mPofT = PofT;
        i = frames; % start at the end
        % set the initial min and minloc to absolute min, should be near
        % the end of the sweep
        %[prevmin, prevminloc] = min( PofT, [], 3 );
        [prevmin, prevminloc] = min( PofT );
        %prevmin = barFofT( h, w, frames );
        while i > 0
            is = i-t2;
            if(is<1)
                is=1;
            end
            %[ fmin, fminloc ] = min( PofT( 1, 1, is:i ), [], 3 );
            [ fmin, fminloc ] = min( PofT(is:i) );
            fminloc = fminloc + is - 1; % adds the offset for indexing the subarray
            dx = i - fminloc; % distance to min, width of event roughly
            if ( dx > t4 ) && ( prevminloc > fminloc ) % ( minflag == true )  % use slope
                slope_dx = prevminloc - fminloc;
                slope_dy = prevmin - fmin;
                slope = slope_dy / slope_dx;
                %x = linspace( 0, slope_dx, slope_dx+1 );
                %xx = x( 1, 1:slope_dx+1 );
                %slope_fill = fmin + xx * slope;
                %slope_fill = fmin + linex( 1, 1:slope_dx+1 ) * slope;
                slope_fill = fmin + linspace(0,slope_dx+1,slope_dx+1) * slope;

                %minBF( h, w, fminloc:prevminloc ) = slope_fill; 
                %mPofT(1,1,fminloc:prevminloc) = slope_fill;
                mPofT(fminloc:prevminloc) = slope_fill;
                %prevmin = fmin;
                %prevminloc = fminloc;
            else % do not use slope
                %minBF( h, w, fminloc:i ) = fmin; 
                %mPofT(1,1,fminloc:i) = fmin;
                mPofT(fminloc:i) = fmin;

            end
                if ( (prevminloc-fminloc) > 2*t4 ) 
                    prevmin = fmin;
                    prevminloc = fminloc;
                    %disp(append('updating prevmin: ',num2str(prevmin), ' , ', num2str(prevminloc)))
                end
            i = fminloc - 1; % this is a time saver, advances to the min value
            %plotpixelsPRO(f,1,1,PofT,mPofT,mPofT);
            %plotPro(f,PofT,mPofT,PofT);
        end % while loop
        %plotpixelsPRO(f,1,1,PofT,mPofT,mPofT);
        %plotPro(f,PofT,mPofT,mPofT);
        %minBF( h, w, : ) = mPofT;
        parstor(p,:) = mPofT;
        %parstor(p,1:10)=PofT(1:10);
end % for or parfor loop
for p=1:pixels
    [h,w]=ind2sub(sz,p);
    minBF(h,w,:)=parstor(p,:);
end
str = append('min bar f of t: ', num2str(toc) );
disp(str);
end
