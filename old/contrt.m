
stack = dFoF0;
cons = 1;
cone = 1800; % 3 min
trts = 2400; % 1 min offset from trt start
trte = 4200; % 3 min window
consubstack = substackTime( stack, cons, cone );
conmax = projectMax( consubstack );
trtsubstack = substackTime( stack, trts, trte );
trtmax = projectMax( trtsubstack );
%imshowpair( conmax, trtmax, 'montage');
figure
subplot(1,2,1);
imshow(conmax);
clim('auto')
subplot(1,2,2);
imshow(trtmax);
clim('auto')
%fitswrite(conmax, 'conmax.fits');
%fitswrite(trtmax, 'trtmax.fits');