function txt = getDataTipsTD(~, info, plane, stack, ifig, dfig)
    %disp(info);
    %disp( append('testcallback: ', num2str(info.Position(1)), num2str(info.Position(2)) ) );
    y = info.Position(1);
    x = info.Position(2);
    %info.target
    %img = getimage(info.target);
    z = plane(x, y);
    txt = ['(' num2str(x) ', ' num2str(y) ', ' num2str(z) ')'];
    plotpixels2f( dfig, stack, x, y );
    figure(ifig)
end