% master
% open dataset
% run openETS, extracts image stack into memory
[ stack, name ] = openETSui(); % puts image stack into 'stack'
%%
trimmedStack = trimArray(stack,3,1,100);
%% clean stack
disp('cleaning stack')
[ outliers, projMax ] = findOutliersMAX( stack, 6 ); % 4.5 x std threshold
cstack = replaceOutliers( trimmedStack, outliers );
%% gaussian BROKEN!
gaussCstack = spatGauss( cstack, 0.5, 5 );
%% run tdResize, bin stack, 4x4 again seems fine!
disp('resize, 4x4 binning')
bin = 2;
st4x4 = tdResize( gaussCstack, bin );
%% run cropstack - project max, cut as small as possible
disp('cropstack')
cst4x4 = cropStack( st4x4 ); % user must right click and crop
%% skip resize in time
%disp('not downsampling in time')
zst = cst4x4; % 
%zst = resizeZ( cst4x4, 10 ); % downsample by 10 frames

%% full frame dF/F0
%% bar F of T
%t1 = 10; % frames to smooth
%bFofT = BarFofT( zst, t1 );
%% min Bar F of T
%t2 = 500; % frames to seek backwards minimum
%minBF = minBarF( bFofT, t2 );
%% dF/F0
disp('dFoF0')
t1=10; % 10s?, frames, 0.1 sec / frame right now!
t2=5000; % frames, should be 500s but MinBarF is really slow! 
t4=20; % frames to define a prominent event for slope fill
[dFoF0,minBF] = deltaFoverFzero( zst, t1, t2, t4 ); % includes t1 and t2 steps above!
% store the analyzed data!
%createH5( dFoF0, name, '/1' );
%% MOVIE TIME!
disp('make a movie')
%%
mj = projectMaxAnalysis(dFoF0,minBF,zst);