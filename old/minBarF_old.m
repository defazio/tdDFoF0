% min bar F
function minBF = minBarF_old( barFofT, t2 )
% for each time point from the end, 
% look backwards in time over t2 to find the minimum barFofT
% t2 = 500;
%height = size( barFofT, 1);
%width = size( barFofT, 2);
frames = size(barFofT, 3);
%minBF = zeros( height, width, frames);
tic
for f = frames : -1 : 1
    fs = f-t2;
    if(fs<1)
        fs=1;
    end
    minBF( :, :, f) = min( barFofT( :, :, fs:f ), [], 3 );
end
str = append('min bar f of t: ', num2str(toc) );
disp(str);
end
