function plotimage( stack )
low = min( stack, [], 'all');
high = max( stack, [], 'all');
figure
imshow( stack, [low high])
end