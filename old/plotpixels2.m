% plot a pixel over time
function plotpixels2( s1, x, y )
frames = size( s1, 3 );
p1 = zeros( frames, 1 );
p1(:,1) = s1( x, y, : );

figure
plot(p1)
end