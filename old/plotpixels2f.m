% plot a pixel over time
function plotpixels2f( f, s1, x, y )
frames = size( s1, 3 );
p1 = zeros( frames, 1 );
p1(:,1) = s1( x, y, : );

figure(f)
plot(p1)
end