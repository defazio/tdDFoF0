% plot a pixel over time
function plotpixelsPRO( f, x, y, s1, s2, s3 )
frames = size( s1, 3 );
p1 = zeros( frames, 3 );
p1(:,1) = s1( x, y, : );
% p2 = zeros(frames);
p1(:,2) = s2( x, y, : );
p1(:,3) = s3(x,y,:);
figure(f)
plot(p1)
end