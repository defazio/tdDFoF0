function cleaned = replaceOutliers( A, outliers )
    % out is a logical array identifying outliers to limit search
    tic
    sx = size( A, 1 );
    sy = size( A, 2 );
    % sz = size( A, 3 );
    cleaned = A; %zeros( sx, sy, sz, 'uint16');
    %figure
   %v = zeros(1,sz);
    for i = 1 : sx 
        for j = 1 : sy
            if outliers(i,j) == 1
                v = double( A( i, j, : ) );
                vc = filloutliers( v, 'linear' );
                cleaned( i, j, : ) = uint16( vc );
                %plotMultiY(v,vc);
                %pause
            end
        end
    end
    disp(append('replace outliers...', num2str(toc)));
end
