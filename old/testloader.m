ets = '/Volumes/GoogleDrive/My Drive/TD CellSens stuff/Folder_20220210/_20220210a_04_/stack1/frame_t_0.ets';
name = '20220210_a_m0_kiss.h5';
% data = bfopen(ets); % creates cell array, frames x 2
series1 = data{1,1}; % move cell containing frame cells
height = size( series1{1,1}, 1 );
width = size( series1{1,1}, 2 );
frames = size( data{1,1}, 1);
h5create( name, '/1', [height width frames], 'datatype', 'uint16');
series1m = zeros(height, width, frames, 'uint16');
for p = 1 : frames
     series1c = series1{p,1}; % moves a frame to temp variable
     series1m(:,:,p) = series1c; % moves temp variable containing frame to h5 structure
%     h5write( name, '/1', series1c );
end
h5write( name, '/1', series1m );
h5disp(name);
