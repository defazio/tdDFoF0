%% dF/F0
disp('dFoF0')
t1=50; % 10s?, frames, 0.1 sec / frame right now!
t2=inf; % frames, should be 500s but MinBarF is really slow! 
t4=200; % frames to define a prominent event for slope fill
[odFoF0,ominBF] = olddeltaFoverFzero( stack, t1, t2 ); % includes t1 and t2 steps above!
% store the analyzed data!
%createH5( dFoF0, name, '/1' );
%% smooth stack
osDFoF0 = SGtStack( odFoF0, 3, 9 );
%% MOVIE TIME!
disp('make a movie')
movieTime( osDFoF0 );
%%
omj = projectMaxAnalysis(osDFoF0,ominBF,stack);