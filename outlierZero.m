function txt = outlierZero(~, info, plane, stack, ifig)
    %disp(info);
    %disp( append('testcallback: ', num2str(info.Position(1)), num2str(info.Position(2)) ) );
    y = info.Position(1);
    x = info.Position(2);
    %info.target
    %img = getimage(info.target);
    meanround = -plane(x, y);
    for i = x-1:1:x+1
        for j = y-1:1:y+1
            meanround = meanround + plane(i, j);
        end
    end
    meanround = meanround/8; %replace outliers with the surrounding 8 pixels.

    %Need more works on finding the exact outlier 
    if plane(x, y)>=meanround+500
        z=meanround;
    else
        z=plane(x, y);
    end
    txt = ['(' num2str(x) ', ' num2str(y) ', ' num2str(z) ')'];

    figure(ifig)
end