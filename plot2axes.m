function p1 = plot2axes( a1, a2, a3, a4, x, y )
% a are 3d arrays, x-y-t; this plots the point x,y 
f = size(a1,3);
t = linspace(0,f);
%p1 = linspace( 0, f );
p1=zeros(f,3);
p1(:,1) = a1( x, y, : );
p1(:,2) = a2(x,y,:);
p1(:,3) = a3(x,y,:);
%p2 = linspace( 0, f );
p2(:,1) = a4( x, y, : );

figure
yyaxis left
plot( p1 )
yyaxis right
plot( p2 )
end