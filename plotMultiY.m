function plotMultiY( y1, y2 )
npts = size(y1,3);
p = zeros(npts,2);
x = linspace(1,npts,npts);
yyaxis left
plot(x,y1);
yyaxis right
plot(x,y2);
end