% plot a pixel over time
function plotpixelsXY( ifig, dfig, imMat, x, y )
% ifig is the image figure handle
% dfig is the profile plot figure handle
% img is the image for the xy data
% x and y indicate where the user clicked in img

[ xpnts, ypnts ] = size( imMat );

xx = linspace( 1, xpnts, xpnts );
yy = linspace( 1, ypnts, ypnts );
p1 = zeros( ypnts, 1 );
p1(:,1) = imMat( x, : );
p2 = zeros( xpnts, 1 );
p2(:,1) = imMat( : , y );

figure(dfig)
yyaxis left
plot(yy, p1)
yyaxis right
plot(xx, p2)

%p1mean = mean(p1,1);
%p2mean = mean(p2,1);
%p1max = max(p1,[], 1);
%p2max = max(p2,[], 1);
%disp("test");
%disp(append("p1 mean: ", num2str(p1mean), "p1 max: ", num2str(p1max), "p2 mean: ", num2str(p2mean), "p2 max: ", num2str(p2max)))

%[ eightPairs ] = localfwhm2D( im, x, y, 20 );
imMatRot = imrotate( imMat, 360 );
imMatRotRot = imrotate( imMatRot, 360 );
xxx = localFWHdelta4D( imMatRotRot, imMatRot, x, y, 20 );
%disp("back to plotpixels")
disp( append( " coords ", num2str( eightPairs ) ) );
figure(ifig)
h = images.roi.Freehand(gca,'Position', eightPairs);

end