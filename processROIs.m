function ROIstats = processROIs(ROIstats)
t1 = 50;
t2 = inf;
t4 = 2000; % need to upgrade to time, not steps

n=size(ROIstats,2); % number of ROIs
t=size(ROIstats(1).ROI.meanZ,2);
stack = zeros(2,n,t);
for i=1:n
    raw = ROIstats(i).ROI.meanZ;
    pen = ROIstats(i).penumbra.meanZ;
    trace = raw - pen;
    stack(1,i,:) = raw; % position 1 of stack is type RAW, position 2 is roi
    stack(2,i,:) = trace; % position 1 of stack is type RAW-sub pen, position 2 is roi
end

[dFoF0,minBF] = deltaFoverFzero(stack,t1,t2,t4);

for i=1:n
    ROIstats(i).dFoF0.ROI = squeeze(dFoF0( 1, i, : ));
    ROIstats(i).dFoF0.pensub = squeeze(dFoF0( 2, i, : ));
end

end