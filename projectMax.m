function MaxProj = projectMax( stack )
% max projection, sd projection?
tic
MaxProj = max( stack, [], 3 );
%MaxProj2 = histeq( MaxProj );

disp(append('projectMax...',num2str(toc)));
end
