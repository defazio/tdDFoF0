%% read dFoF0 from file
% HDF5 .h5 and/or matlab .m
% move to h5 format for processing
function stack = readH5( tag )
tic
[file,path] = uigetfile('*.h5');
if isequal(file,0)
   disp('User selected Cancel');
else
   disp(['User selected ', fullfile(path,file)]);
   stack = h5read( fullfile(path,file), tag ); 
end

disp(append('readH5 ', num2str(toc), ' s' ));
end
