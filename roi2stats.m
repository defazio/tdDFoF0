function ROIstats = roi2stats( im, h )
dx = size( im, 1 );
dy = size( im, 2 );
% binary image %% stolen from image analyst (again)
binaryImage = h.handles.ROI.createMask();
% Calculate the area, in pixels, that they drew.
numberOfPixels1 = sum(binaryImage(:));
% Another way to calculate it that takes fractional pixels into account.
numberOfPixels2 = bwarea(binaryImage);
% Mask the image and display it.
% Will keep only the part of the image that's inside the mask, zero outside mask.
blackMaskedImage = im;
blackMaskedImage(~binaryImage) = 0;
% Calculate the mean
meanGL = mean(blackMaskedImage(binaryImage));
ROIstats.ROI.mean = meanGL;
ROIstats.ROI.area1 = numberOfPixels1;
ROIstats.ROI.area2 = numberOfPixels2;
end