function ROIstats = roi2stats3D( imStack, h )
% dx = size( imStack, 1 );
% dy = size( imStack, 2 );
dz = size( imStack, 3 );
% binary image %% stolen from image analyst (again)
roiImage = h.handles.ROI.createMask();
deadzoneImage = h.handles.deadzone.createMask();
penumbraImage = h.handles.penumbra.createMask();
% Calculate the area, in pixels, that they drew.
numPixROI = sum(roiImage(:));
numPixDead = sum(deadzoneImage(:));
numPixPen = sum(penumbraImage(:));
% Another way to calculate it that takes fractional pixels into account.
numberOfPixels2 = bwarea(roiImage);
% Mask the image and display it.
% Will keep only the part of the image that's inside the mask, zero outside mask.
% Calculate the mean
%meanZ = zeros(dz,1);
%slice = zeros(dx,dy);

for i=1:dz
    sliceROI = imStack(:,:,i);
    sliceDead = imStack(:,:,i);
    slicePen = imStack(:,:,i);
    val = NaN;
    sliceROI(~roiImage) = val; % wipe out everything but the ROI
    sliceDead(~deadzoneImage) = val; % wipe out everything outside deadzone
    sliceDead(roiImage) = val; % wipe out inside ROI to make donut
    slicePen(~penumbraImage) = val; % wipe out outside penumbra
    slicePen(deadzoneImage) = val;  % wipe out inside deadzone to make 
                                    % penumbra donut
    
    meanROI = mean(sliceROI(roiImage));
    meanDeadzone = mean(sliceDead(deadzoneImage), 'omitnan');
    meanPenumbra = mean(slicePen(penumbraImage), 'omitnan');
    ROIstats.ROI.meanZ(i) = meanROI ;
    ROIstats.deadzone.meanZ(i) = meanDeadzone ;
    ROIstats.penumbra.meanZ(i) = meanPenumbra ;
end
ROIstats.ROI.area1 = numPixROI;
ROIstats.deadzone.area1 = numPixDead;
ROIstats.ROI.area1 = numPixPen;
ROIstats.ROI.area2 = numberOfPixels2;
end