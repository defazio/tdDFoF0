function eightPairs = testimprofile( im, R0, C0, r )
% matlab (row,column) = (x,y) % origin is top left
[ Rpnts, Cpnts ] = size(im);
Rmin = R0 - r;
if(Rmin<1)
    Rmin = 1;
end
Rmax = R0 + r;
if(Rmax>Rpnts)
    Rmax = Rpnts;
end
Cmin = C0 - r;
if(Cmin<1)
    Cmin = 1;
end
Cmax = C0 + r;
if(Cmax>Cpnts)
    Cmax = Cpnts;
end
eightPairs = zeros(8,2);
% vertical, v
Ccoord = [ Rmin, Rmax ];
Rcoord = [ C0, C0 ];
[ vRcoord, vCcoord, vprofile ] = improfile( im, Rcoord, Ccoord );
twoPairs = localFWHdelta1D( vRcoord, vCcoord, vprofile );
eightPairs(1,1) = twoPairs(1,2);
eightPairs(5,1) = twoPairs(2,2);
eightPairs(1,2) = twoPairs(1,1);
eightPairs(5,2) = twoPairs(2,1);

% horizontal, h
Ccoord = [ R0, R0 ];
Rcoord = [ Cmax, Cmin ];
[ vRcoord, vCcoord, vprofile ] = improfile( im, Rcoord, Ccoord );
twoPairs = localFWHdelta1D( vRcoord, vCcoord, vprofile );
eightPairs(3,1) = twoPairs(1,2);
eightPairs(7,1) = twoPairs(2,2);
eightPairs(3,2) = twoPairs(1,1);
eightPairs(7,2) = twoPairs(2,1);

% +45 degrees, p
Ccoord = [ Rmin, Rmax ];
Rcoord = [ Cmax, Cmin ];
[ vRcoord, vCcoord, vprofile ] = improfile( im, Rcoord, Ccoord );
twoPairs = localFWHdelta1D( vRcoord, vCcoord, vprofile );
eightPairs(2,1) = twoPairs(1,2);
eightPairs(6,1) = twoPairs(2,2);
eightPairs(2,2) = twoPairs(1,1);
eightPairs(6,2) = twoPairs(2,1);
% -45 degrees, m
Ccoord = [ Rmax, Rmin ];
Rcoord = [ Cmax, Cmin ];
[ vRcoord, vCcoord, vprofile ] = improfile( im, Rcoord, Ccoord );
twoPairs = localFWHdelta1D( vRcoord, vCcoord, vprofile );
eightPairs(4,1) = twoPairs(1,2);
eightPairs(8,1) = twoPairs(2,2);
eightPairs(4,2) = twoPairs(1,1);
eightPairs(8,2) = twoPairs(2,1);


end