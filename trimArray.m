%trim frames
function trimmed = trimArray( array, dim, startindex, endindex ) 
tic 
trimmed = array;
trimmed(:,:, startindex : endindex )=[];
str = append('trimming took ', num2str(toc), ' seconds' );
disp(str);
end